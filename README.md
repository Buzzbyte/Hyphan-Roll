# Hyphan-Roll

The Hyphan-Roll mod is a HyphanBot mod that allows users to set a list of words
or phrases in a chat to be randomly rolled on command.

This mod's roll list will configured per-chat.

The mod's settings, including the default list, will be configured through the
configuration file.

## Installation
The installation commands are simple. Just clone, permit, and install.
```bash
$ git clone https://gitlab.com/NerdyBuzz/Hyphan-Roll.git
$ cd Hyphan-Roll/
$ chmod +x install
$ ./install
```

## Commands
There are two main Telegram commands for this mod:
* **/set-roll** - Defines the roll list. Each item is separated by a pipe
character (`|`).
```
/set-roll Yes | No | Maybe
```

* **/roll** - Chooses a random item from the list defined using */set-roll*.
This command can be called with an optional argument which wont have an effect
on the random outcome.
```
/roll
/roll Did I do well?
```

## License
Same as [HyphanBot's Licence](https://www.gnu.org/licenses/agpl-3.0.html).
import random
from telegram.ext import CommandHandler

class Roll:
    """
    Handles the functionality for the Roll mod.
    
    Args:
        storage (hyphanbot.Storage): HyphanBot's storage object.
    """
    def __init__(self, storage):
        self.storage = storage

    def set_roll(self, bot, update, args):
        args = " ".join(args).split("|")
        args = [arg.strip() for arg in args]
        chatid = update.message.chat_id
        self.storage.set(chatid, "roll_list", args)
        bot.sendMessage(chat_id=chatid, text="Roll list was set for this chat.")

    def roll(self, bot, update):
        chatid = update.message.chat_id
        try:
            roll_list = self.storage.get(chatid, "roll_list")
            choice = random.choice(roll_list)
            bot.sendMessage(chat_id=chatid, text=choice)
        except KeyError:
            bot.sendMessage(chat_id=chatid, text="No roll list was set for this chat.")
        except:
            bot.sendMessage(chat_id=chatid, text="Unknown error occured.")

class Dispatch:
    """
    Dispatches the mod's commands to be handled by Telegram.
    
    Args:
        api (hyphanbot.API): HyphanBot's API object.
        updater (telegram.ext.Updater): The Updater object to handle Telegram
            message updates.
    """
    def __init__(self, api, updater):
        disp = updater.dispatcher
        storage = Storage()
        roll = Roll(storage)

        disp.add_handler(CommandHandler("set_roll", roll.set_roll, pass_args=True))
        disp.add_handler(CommandHandler("roll", roll.roll))

        api.set_help("set_roll", "Defines a list to randomly choose from.")
        api.set_help("roll", "Randomly chooses from defined list.")

class Storage:
    """
    Used to simulate HyphanBot's future API feature.
    """
    def __init__(self):
        self.stored = {}
        self.stored[__name__] = {}

    def set(self, chat_id, key, value):
        if chat_id in self.stored[__name__]:
            self.stored[__name__][chat_id][key] = value
        else:
            self.stored[__name__][chat_id] = {key: value}

    def get(self, chat_id, key):
        return self.stored[__name__][chat_id][key]

